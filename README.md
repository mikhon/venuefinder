A study project to show off some REST API & Android service usage and to try out some MVP architecture and unit testing stuff.

* Summary of set up
    * Clone and pull Volley and open it in Eclipse, set as "library"
    * Clone and pull VenueFinder & create new project from existing code in Eclipse. Add Volley to VenueFinder-project as a library
* Dependencies
    * Volley
    * Foursquare REST API
* How to run tests
    * Manually in device
    * Manually under Eclipse