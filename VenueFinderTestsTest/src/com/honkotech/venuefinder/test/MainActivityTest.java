package com.honkotech.venuefinder.test;

import com.honkotech.venuefinder.MainActivity;
import com.honkotech.venuefinder.R;
import com.honkotech.venuefinder.Venue;
import com.honkotech.venuefinder.VenueList;

import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

/**
 * This class contains the unit tests to verify basic functionality of data models ({@link Venue} & {@link VenueList}) and the view (MainActivity & CustomAdapter)
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private MainActivity mActivity;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mActivity = getActivity();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public MainActivityTest(Class<MainActivity> activityClass) {
        super(activityClass);
    }

    public MainActivityTest() {
        super(MainActivity.class);
    }

    public void testCase1() {
        // this test case verifies that the view has basic views
        TextView testTextView = (TextView)mActivity.findViewById(R.id.editText1);
        assertNotNull("editText was null", testTextView);
        ListView listView = (ListView)mActivity.findViewById(R.id.listView1);
        assertNotNull("listView1 was null", listView);
        TextView emptyView = (TextView)mActivity.findViewById(R.id.empty);
        assertNotNull("emptyView was null", emptyView);
    }

    public void testCase2() {
        // this test case verifies that the Venue class works properly
        Venue venue = new Venue("Pub Neidonkenkä", 100.0, "Kiiminki");
        assertEquals("100", venue.getDistance());
        assertEquals("Pub Neidonkenkä", venue.getName());
        assertEquals("Kiiminki", venue.getAddress());
    }

    public void testCase3() {
        // this test case verifies that the VenueList class works properly
        VenueList testList = createTestVenueList();
        assertEquals(2, testList.size());
        testList.emptyList();
        assertEquals(0, testList.size());
    }

    public void testCase4() throws Throwable {
        /* This test case verifies that the custom adapter delivers the contents of the model
         * properly into the list view */
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                VenueList testList = createTestVenueList();
                mActivity.setItems(testList);
            }
        });

        getInstrumentation().waitForIdleSync(); // wait until the views are ready
        ListView listView = (ListView)mActivity.findViewById(R.id.listView1);
        View listItemView = (View)listView.getChildAt(0);
        TextView nameView = (TextView) listItemView.findViewById(R.id.venue_name);
        TextView addrView = (TextView) listItemView.findViewById(R.id.venue_address);
        TextView distView = (TextView) listItemView.findViewById(R.id.venue_distance);
        assertEquals("Pub Neidonkenkä", nameView.getText());
        assertEquals("Kiiminki", addrView.getText());
        assertEquals("100m", distView.getText());
    }

    private VenueList createTestVenueList() {
        VenueList venueList = new VenueList();
        Venue venue = new Venue("Pub Neidonkenkä", 100.0, "Kiiminki");
        venueList.addVenue(venue);
        venue = new Venue("Pub Graali", 20000.0, "Albertinkatu, Oulu");
        venueList.addVenue(venue);
        return venueList;
    }

}