package com.honkotech.venuefinder.test;

import com.honkotech.venuefinder.Venue;
import com.honkotech.venuefinder.VenueList;
import com.honkotech.venuefinder.VenueParser;

import java.io.IOException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;
import android.test.InstrumentationTestCase;

public class VenueParserTest extends InstrumentationTestCase {

    public VenueParserTest() {
        super();
    }

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Verifies that the parser works with expected input
     * @throws JSONException
     */
    public void testParseResponse() throws JSONException {
        VenueParser parser = new VenueParser();
        String temp = readFile("test.json");
        JSONObject response = new JSONObject(temp);
        VenueList venues = parser.parseResponse(response);
        Venue venue = (Venue)venues.getList().get(0);
        assertEquals("Villenniemen venesatama", venue.getName());
        assertEquals("[\"Villentie\",\"90850 Martinniemi, Haukipudas\",\"Finland\"]", venue.getAddress());
        assertEquals("986", venue.getDistance());
    }

    /**
     * Verifies that the parser works when no venues are found
     * @throws JSONException
     */
    public void testEmptyResponse() throws JSONException {
        VenueParser parser = new VenueParser();
        String temp = readFile("empty.json");
        JSONObject response = new JSONObject(temp);
        VenueList venues = parser.parseResponse(response);
        assertEquals(0, venues.size());
    }

    // TODO testBadResponse(), containing some unexpected data

    public String readFile(String name) {
        String json = null;
        try {
            InputStream is = getInstrumentation().getContext().getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
