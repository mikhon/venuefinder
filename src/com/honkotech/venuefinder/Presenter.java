package com.honkotech.venuefinder;

import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ClearCacheRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.util.Log;

/*
 * This class handles the presenter-part. It reacts on user input that's forwarded from the MainActivity
 * and uses the MainActivityInterface to set the results into the list.
 * We wait for 2 seconds after each keypress to limit the amount of unnecessary requests to Foursquare
 * */

public class Presenter implements PresenterInterface {
    private static final String TAG = "VenueFinder - Presenter";
    private static final boolean TEST_DEBUG = false;
    private static final String CLIENT_ID = "35TQZLBWRQ0XKLJSIKNYKZN4EZQYC3IICQMYEKILWVNBKQ1Y";
    private static final String CLIENT_SECRET = "F4IJW4LWBFIY3NR2GYGU4IMNYLGKM0RHADNSPJO2PEFDRIV0";
    private static final String VERSION = "20130815";
    private boolean mLocationFailed = false;
    private MainActivityInterface mainActivity;
    private static RequestQueue mQueue;
    Handler timerHandler = new Handler();
    private final Object mTag = this;
    private double mLat = 0.0;
    private double mLon = 0.0;
    private String searchText = "";
    private VenueList mVenueList = new VenueList();
    private VenueParser mParser = new VenueParser();

    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            mainActivity.showWaitNote();
            String url ="https://api.foursquare.com/v2/venues/search?client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET
                    + "&v=" + VERSION + "&ll=" + mLat + "," + mLon + "&query=" + searchText;
            Log.d(TAG, "url: " + url);
            JsonObjectRequest foursquareRequest = new JsonObjectRequest(Request.Method.GET, url,
                        null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    if (TEST_DEBUG) Log.d(TAG,"Foursquare server response: " + response);
                    // Parse the response.
                    mVenueList = mParser.parseResponse(response);
                    mainActivity.setItems(mVenueList);
                    mainActivity.hideWaitNote();
                }
            }, new Response.ErrorListener() {
                @Override

                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG,"Error response from remote");
                    mainActivity.enableInput();
                    mainActivity.hideWaitNote();
                    // TODO show another error note?
                }
            });

            foursquareRequest.setTag(mTag);
            Log.d(TAG,"Request added to queue");
            // Clear old results from view and model
            mVenueList.emptyList();
            mainActivity.setItems(null);
            mQueue.add(foursquareRequest);
        }
    };

    /**
     * Presenter controls the mainActivity (view), handles fetching data from the venue service and maintains the models (Venue & VenueList)
     * The process has 2 steps - first, we wait until we get a GPS fix. After that, once initially and then every time a user types something 
     * in the search box, the presenter fetches matching venues from Foursquare and stores the received list into the model and updates the UI.
     * 
     * @param mainActivity Connected activity (view) must implement MainActivityInterface
     * @param mContext Application context
     */
    public Presenter(MainActivityInterface mainActivity, Context context) {
        this.mainActivity = mainActivity;
        MyLocationProvider provider = new MyLocationProvider();
        mQueue = Volley.newRequestQueue(context);

        /* After a LOT of requests, cache tends to fill up the whole memory 
         * and after that, Volley crashes (in Device Under Test, Nokia X2) */
        mQueue.add(new ClearCacheRequest(mQueue.getCache(), null));

        /* Disable input initially so the user can't type before GPS fix is OK
         * as info text is shown in the text field */
        mainActivity.disableInput();
        provider.getLocation(this, context);
    }

    @Override
    public void onInputChanged(Editable editable) {
        Log.d(TAG, "onInputChanged(), editable text: " + editable.toString());
        searchText = editable.toString();
        // cancel possible ongoing request and reset timer
        timerHandler.removeCallbacks(timerRunnable);
        mQueue.cancelAll(mTag);
        if (!TEST_DEBUG && !mLocationFailed) {
            timerHandler.postDelayed(timerRunnable, 2000); // send the request after 2 seconds idle in user input
        } else if (!mLocationFailed) {
            // launch immediately to speed up UI testing
            timerHandler.postDelayed(timerRunnable, 0);
        } else {
            // TODO show error banner to user that this won't work without GPS
        }
    }

    // This is called by the Main activity when it's stopped to make sure there's no pending requests
    @Override
    public void cancelAll() {
        timerHandler.removeCallbacks(timerRunnable);
        mQueue.cancelAll(mTag);
    }

    @Override
    public void setLocation(double longitude, double latitude, boolean success) {
        if (success) {
            mLon = longitude;
            mLat = latitude;
            timerHandler.postDelayed(timerRunnable, 0);
            mainActivity.showWaitNote();
            mainActivity.clearInput();
            mainActivity.enableInput();
            mLocationFailed = false;
        } else {
            mainActivity.hideKeyboard();
            mLocationFailed = true;
            mainActivity.showError();
            mainActivity.disableInput();
            Log.e(TAG, "Location was null, can't work!");
        }
    }
}
