package com.honkotech.venuefinder;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Adapter to map venues into list items. <br>
 * Maps venue name, distance and address into respective text fields in the list item.<br>
 * Extends {@link ArrayAdapter}
 */
public class CustomAdapter extends ArrayAdapter<Venue> {
    private final Context mContext;
    private final List<Venue> venueList;
    private View itemView;

    /**
     * @param context Activity context
     * @param list List containing venues
     */
    public CustomAdapter(Context context, List<Venue> list) {
      super(context, R.layout.list_item);
      mContext = context;
      venueList = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      LayoutInflater inflater = (LayoutInflater) mContext
          .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      if (convertView == null) {
          itemView = inflater.inflate(R.layout.list_item, parent, false);
      } else {
          itemView = convertView;
      }
      TextView nameView = (TextView) itemView.findViewById(R.id.venue_name);
      TextView distanceView = (TextView) itemView.findViewById(R.id.venue_distance);
      TextView addressView = (TextView) itemView.findViewById(R.id.venue_address);

      Venue venue = (Venue)venueList.get(position);
      nameView.setText(venue.getName());
      distanceView.setText(venue.getDistance() + "m");
      String addr = venue.getAddress().replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "");
      addressView.setText(addr);

      return itemView;
    }

    @Override
    public int getCount() {
        if (venueList != null) {
            return venueList.size();
        } else {
            return 0;
        }
    }
}
