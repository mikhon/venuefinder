package com.honkotech.venuefinder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * This class provides a service to parse the received JSON response into a list of {@link Venue}s in a {@link VenueList}.
 * 
 */
public final class VenueParser {
    private static final String TAG = "VenueFinder - VenueParser";
    private VenueList mVenueList = new VenueList();

    /**
     *  This function parses a JSON formatted response from the server, finds venues and adds them into VenueList
     *  @param response Type {@link JSONObject}
     *  @return {@link VenueList}
     *  */
    public VenueList parseResponse(JSONObject response) {
        String name = "";
        String addr = "";
        double distance = 0.0;

        try {
            JSONArray venues = response.getJSONObject("response")
                    .getJSONArray("venues");
          for (int i = 0; i<venues.length();i++){
              JSONObject obj = venues.getJSONObject(i);
              name = obj.getString("name");
              JSONObject location = obj.getJSONObject("location");
              Log.d(TAG, "location: " + location.toString(4));
              if (location.has("formattedAddress")) addr = location.getString("formattedAddress");
              if (location.has("distance")) distance = location.getDouble("distance"); 
              Log.d(TAG, "venue name: " + name);
              Log.d(TAG, "venue addr: " + addr);
              Log.d(TAG, "venue distance: " + distance);
              Venue venue = new Venue(name, distance, addr);
              mVenueList.addVenue(venue);
          }

        } catch (JSONException e) {
            Log.d(TAG, "JSONArray parsing failed!");
            e.printStackTrace();
        }
        return mVenueList;
    }
}
