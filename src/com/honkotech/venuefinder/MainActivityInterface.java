package com.honkotech.venuefinder;

public interface MainActivityInterface {

    public void setItems(VenueList venueList);
    public void hideKeyboard();
    public void enableInput();
    public void disableInput();
    public void clearInput();
    public void showError();
    public void showWaitNote();
    public void hideWaitNote();

}
