package com.honkotech.venuefinder;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends Activity implements MainActivityInterface {
    private ListView listView;
    private Presenter presenter;
    private EditText editText;
    TextWatcher watcher= null;
    private ListAdapter adapter;
    private ProgressBar mSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.editText1);
        editText.setText(R.string.waitingText);
        listView = (ListView) findViewById(R.id.listView1);
        listView.setEmptyView(findViewById(R.id.empty));
        mSpinner = (ProgressBar)findViewById(R.id.progressBar);
        presenter = new Presenter(this, getApplicationContext());
        watcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                presenter.onInputChanged(editText.getText());
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                    int after) {
                // Do nothing
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                    int count) {
                // Do nothing
            }
            
        };
        editText.addTextChangedListener(watcher);
    }

    @Override
    public void setItems(VenueList venueList) {
        if (venueList != null) {
            // this is not the optimal solution to handle the data & adapter, but it'll do just fine in this case
            adapter = new CustomAdapter(this,
                    venueList.getList());
            listView.setAdapter(adapter);
        }
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
          imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @Override
    public void enableInput() {
        // TODO Auto-generated method stub
        editText.setEnabled(true);
    }

    @Override
    public void disableInput() {
        editText.setEnabled(false);
    }

    @Override
    public void clearInput() {
        editText.setText("");
    }
    @Override
    public void onStop() {
        presenter.cancelAll();
        super.onStop();
    }

    @Override
    public void showError() {
        TextView emptyView = (TextView) findViewById(R.id.empty);
        emptyView.setText(R.string.error_text);
    }

    @Override
    public void showWaitNote() {
        mSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWaitNote() {
        mSpinner.setVisibility(View.GONE);
    }
}
