package com.honkotech.venuefinder;

import android.text.Editable;

public interface PresenterInterface {

    public void onInputChanged(Editable editable);
    public void cancelAll();
    public void setLocation(double longitude, double latitude, boolean success);

}
