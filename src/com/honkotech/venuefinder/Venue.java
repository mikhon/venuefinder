package com.honkotech.venuefinder;

/**
 * This class contains the relevant data, getters() and setters() for a single venue.
 * Fields: name, address, distance
 */
public class Venue {
    private String name = "";
    private String address = "";
    private double distance = 0.0;
    
    /**
     * @param name Name of the venue
     * @param distance Distance to the venue - note that even though this is <b>double</b>, getDistance()
     * returns a <b>string</b> of its <b>integer</b> value
     * @param addr Addess of the venue 
     */
    public Venue(String name, double distance, String addr) {
        if (name != null && name.length() > 0) this.setName(name);
        if (distance > 0) this.setDistance(distance);
        if (addr != null && addr.length() > 0) this.setAddress(addr);
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    private void setAddress(String address) {
        this.address = address;
    }

    public String getDistance() {
        return String.valueOf((int)distance);
    }

    private void setDistance(double distance) {
        this.distance = distance;
    }
}
