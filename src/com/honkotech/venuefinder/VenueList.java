package com.honkotech.venuefinder;

import java.util.ArrayList;
import java.util.List;

/**
 * This class holds a list of {@link Venue}-objects.
 * Supports adding venues to the list, emptying the complete list and fetching the list and its size.
 */
public final class VenueList {
    private List<Venue> venues = new ArrayList<Venue>();

    public void addVenue(Venue venue) {
        venues.add(venue);
    }

    public void emptyList() {
        venues.clear();
    }

    public List<Venue> getList() {
        return venues;
    }
    
    public int size() {
        return venues.size();
    }
}
