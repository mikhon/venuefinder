package com.honkotech.venuefinder;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * This class provides functionality for the Presenter to get current position data. 
 * Only latitude and longitude are needed.
 * 
 */
public class MyLocationProvider {
    private final String TAG = "VenueFinder - MyLocationProvider";
    private final boolean TEST_DEBUG = false;
    private static final double TEST_LATITUDE = 65.22414509;
    private static final double TEST_LONGITUDE = 25.26267309;

    /**
     * This function finds out where the device is and sets latitude & longitude and result back to the Presenter
     * @param presenter Presenter instance
     * @param mContext Application context
     */
    public void getLocation(final PresenterInterface presenter, Context mContext) {
        if (!TEST_DEBUG) {
            // TODO This could be optimized so that we use the quickest way to get a rough location via WLAN etc.
            LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_LOW);
            criteria.setPowerRequirement(Criteria.POWER_LOW);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setSpeedRequired(false);
            criteria.setCostAllowed(true);
    
            LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        double lat = location.getLatitude();
                        double lon = location.getLongitude();
                        presenter.setLocation(lon, lat, true);
                        Log.d(TAG, "location - lat: " + lat + " lon: " + lon);
                    } else {
                        presenter.setLocation(0, 0, false);
                    }
                }
    
                @Override
                public void onStatusChanged(String provider, int status,
                        Bundle extras) {
                }
    
                @Override
                public void onProviderEnabled(String provider) {
                }
    
                @Override
                public void onProviderDisabled(String provider) {
                }
            };
            locationManager.requestSingleUpdate(criteria, locationListener, null);
        } else { // TEST_DEBUG is enabled, so we return hardcoded results
            presenter.setLocation(TEST_LATITUDE, TEST_LONGITUDE, true);
        }
    }
}
